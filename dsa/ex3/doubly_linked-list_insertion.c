#include<stdio.h>
#include<stdlib.h>

struct node
{
    int data;
    struct node * prev;
    struct node * next;
};


struct node * insertatbeg(struct node * head,int data)
{
    struct node * newnode = (struct node *)malloc(sizeof(struct node));
    newnode->data = data;

    newnode->prev = NULL;
    newnode->next = head;
    
    head->prev = newnode;

    head = newnode;

    return head;
}

struct node * insertatlast(struct node * tail,int data)
{
    struct node * newnode = (struct node *)malloc(sizeof(struct node));
    newnode->data = data;

    newnode->next = NULL;
    newnode->prev = tail;

    tail->next = newnode;

    tail = newnode;

    return tail;
}

struct node * insertatpos(struct node * head,int data,int index)
{
    struct node * new = (struct node *)malloc(sizeof(struct node));

    struct node * temp = head;

    for(int i=1;i<index-1;i++)
    {
        temp = temp->next;
    }
    new->data = data;
    
    new->next = temp->next;
    temp->next->prev = new;
    temp->next = new;
    new->prev = temp;

    head = new;

    return head;
    
}

int print(struct node * head)
{
    struct node * temp = head;
    while(temp!=NULL)
    {
        printf("element %d\n",temp->data);
        temp = temp->next;
    }
    printf("\n");
}

int main()
{
    struct node * head;
    struct node * second;
    struct node * tail;

    head = (struct node *)malloc(sizeof(struct node));
    second = (struct node *)malloc(sizeof(struct node));
    tail = (struct node *)malloc(sizeof(struct node));

    head->data = 10;
    head->prev = NULL;
    head->next = second;

    second->data = 20;
    second->prev = head;
    second->next = tail;

    tail->data = 30;
    tail->prev = second;
    tail->next = NULL;

    head = insertatbeg(head,40); 
    print(head);
    head = insertatbeg(head,50); 
    print(head);
    head = insertatpos(head,2,60);
    print(head);

    return 0;
}
