#include<stdio.h>
#include<stdlib.h>

struct node
{
    int data;
    struct node * next;
    struct node * prev;
};

struct node * deletionatbeg(struct node * head)
{
    struct node * temp = head;

    head = head->next;
    temp->next = NULL;
    head->prev = NULL;
    free(temp);

    return head;
}

struct node * deletionatpos(struct node * head,int index)
{
    struct node * temp = head;

    for(int i=1;i<index-1;i++)
    {
        temp = temp->next;
    }
    temp->next = temp->next->next;
    temp->next->prev = temp;

    head = temp;

    return head;
}

struct node * deletionatlast(struct node * last)
{
    struct node *temp = last;

    last = last->prev;
    temp->prev = NULL;
    last->next = NULL;
    free(temp);

    return last;
}

int print(struct node * head)
{
    struct node * ptr = head;

    while(ptr!=NULL)
    {
        printf("%d\n",ptr->data);
        ptr = ptr->next;
    }
    printf("\n");
}

int main()
{
    struct node *head,*second,*tail,*last;
    
    head = (struct node *)malloc(sizeof(struct node));
    second = (struct node *)malloc(sizeof(struct node));
    tail = (struct node *)malloc(sizeof(struct node));
    last = (struct node *)malloc(sizeof(struct node));


    head->data = 10;
    head->prev = NULL;
    head->next = second;

    second->data = 20;
    second->prev = head;
    second->next = tail;

    tail->data = 30;
    tail->prev = second;
    tail->next = last;

    last->data = 40;
    last->prev = tail;
    last->next = NULL;

    printf("the doubly linked list\n");
    print(head);
    printf("after deletion of first node\n");
    head = deletionatbeg(head);
    print(head);
    
    //error in this one
    //printf("after deletion of last node\n");
    //head = deletionatlast(last);
    //print(head);

    printf("after deletion of pos\n");
    head = deletionatpos(head,2);
    print(head);

    return 0;

}
