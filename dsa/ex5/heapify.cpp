#include<iostream>

using namespace std;

void swap(int *a,int *b) {
	int temp = *a;
 	*a = *b;
	*b = temp;
}

void heapify(int arr[],int size_heap,int root) {

	int largest = root;
	int left_child  = 2*root+1;
	int right_child = 2*root+2;

	if(left_child < size_heap && arr[left_child] > arr[largest]) {
		largest = left_child;	
	}
	
	if(right_child > size_heap && arr[right_child] > arr[largest]) {
		largest = right_child;
	}

	if(largest!=root) {
		swap(&arr[root],&arr[largest]);
		heapify(arr,size_heap,largest);
	}
}

void heapSort(int arr[],int size) {

	//building heap
	for(int i = size/2 - 1;i>=0 ;i++) {
		heapify(arr,size,i);
	} 

	//one by one extract an ele from heap

	for(int i = size-1;i>0;i--) {
		swap(arr[0],arr[i]);
		heapify(arr,i,0);
	}
}

void printarray(int arr[],int size) {
	int i;
	for(i =0;i<size;i++) {
		cout<<arr[i]<< " ";
		cout<<"\n";
	}
}


int main() {

	int arr[] = {12,11,13,5,6,7};
	int size_arr = sizeof(arr)/sizeof(arr[0]);

	heapSort(arr,size_arr);

	cout<<"sorted array \n";

	printarray(arr,size_arr);

	return 0;

}

