#include<stdio.h>

int main()
{
    int a=1,b=2,c=3;
    int *p,*q; //allocate three int and two ptr

    //set p to refer a
    p = &a;

    q=&b;
    printf("%d , %d \n",*p,*q);
    //now mixup some things
    c = *p;
    p=q;
    *p = 13;
    printf("%d , %d\n",c,*q);

    return 0;
}
