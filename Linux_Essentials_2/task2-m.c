#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<errno.h>
#include<sys/wait.h>


int main(){

    int pid1;

    pid1 = fork();   /* Duplicate process. Child and parent continue from here */

    if(pid1 == 0)
    {
        printf("im the first child pid %d and my ppid %d\n",getpid(),getppid());
    }
    else
    {
        if(fork() == 0)
        {
            printf("im the child pid %d of child ppid %d\n",getpid(),getppid());
        }
        else
        {
            printf("im the parent pid %d and my ppid %d\n",getpid(),getppid());
        }
    }
    return 0;
}
